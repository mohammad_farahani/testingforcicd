using System;

namespace dotnetcore
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var arr1 = "HelloMother";
            Console.WriteLine(arr1.HasColumnNameToMySql());
            Console.ReadKey();
        }
    }
    public static class TypeHelper
    {
        public static string HasColumnNameToMySql(this string columnName)
        {
            var result = "";
            for (int i = 0; i < columnName.Length; i++)
            {
                var ch = columnName[i];
                if (Char.IsLower(ch)) // check whether the character is lowercase
                    result += Char.ToLower(ch).ToString(); // Converts lowercase character to uppercase.
                else
                {
                    if (i == 0)
                        result += Char.ToLower(ch).ToString(); // Converts lowercase character to uppercase.
                    else
                        result += "_" + Char.ToLower(ch).ToString(); // Converts lowercase character to uppercase.
                }
            }
            return result;
        }
    }
}
